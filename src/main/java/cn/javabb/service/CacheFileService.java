package cn.javabb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.mapred.gethistory_jsp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.javabb.entity.CacheFile;
import cn.javabb.mapper.CacheFileMapper;
@Service
public class CacheFileService {

    @Autowired
    CacheFileMapper cacheFileMapper;

    
    public List<CacheFile> listByFolderName(String folderName,Integer level){
            CacheFile floder = new CacheFile();
            floder.setServerFilename(folderName);
            floder.setIsdir(1);
            List<CacheFile> floderList = cacheFileMapper.select(floder);
            if(null!=floderList){
                CacheFile floderNew = floderList.get(0); 
                List<CacheFile> menuList = cacheFileMapper.listByParentPath(floderNew.getParentPath()+floderNew.getServerFilename()+"/");
                if(null == menuList){
                    System.out.println("资源目录为空");
                    return null;
                }
                for(CacheFile cf:menuList){
                    if(cf.getServerFilename().indexOf("下载必看")>-1&&cf.getIsdir()==0){
                        continue;
                    }
                    System.out.println(formatter(cf.getServerFilename()));
                    if(cf.getIsdir()==0){
                        
                    }else{
                        if(level>1){
                            listByFolderName(cf.getServerFilename(),level-1);
                        }
                    }
                }
            }else{
                System.out.println("资源找不到");
            }
        
        return null;
    }
    public String formatter(String name){
        String str = name.replace("[www.javadaba.com Java大巴]","")
            .replace("【www.javadaba.com Java大巴】", "")
            .replace("www.javadaba.com", "");
        return str;
    }
    public static void main(String[] args) {
        String str = "[www.java.com 打啊]违法艾丝凡".replaceAll("\\[\\w*?\\]","");
        System.out.println(str);
    }
}
