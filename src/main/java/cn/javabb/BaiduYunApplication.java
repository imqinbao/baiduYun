package cn.javabb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import tk.mybatis.spring.annotation.MapperScan;
@MapperScan(basePackages = {
    "cn.javabb.mapper"
})
@SpringBootApplication
public class BaiduYunApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaiduYunApplication.class, args);
	}
}
