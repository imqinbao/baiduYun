package cn.javabb.entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="cache_file")
public class CacheFile {

    @Id
    private Integer id;
    
    private String parentPath;
    
    private String serverFilename;
    
    private Integer fileSize;
    
    private String md5;
    
    private Integer isdir;
    
    private Integer category;

    public Integer getId() {
        return id;
    }

    public String getParentPath() {
        return parentPath;
    }

    public String getServerFilename() {
        return serverFilename;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public String getMd5() {
        return md5;
    }


    public Integer getIsdir() {
        return isdir;
    }

    public Integer getCategory() {
        return category;
    }

    public void setIsdir(Integer isdir) {
        this.isdir = isdir;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public void setServerFilename(String serverFilename) {
        this.serverFilename = serverFilename;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
    
    
}
