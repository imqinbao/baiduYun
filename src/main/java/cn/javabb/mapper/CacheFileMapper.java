package cn.javabb.mapper;

import java.util.List;
import java.util.Map;

import cn.javabb.common.BaseMapper;
import cn.javabb.entity.CacheFile;

public interface CacheFileMapper extends BaseMapper<CacheFile>{

    public List<CacheFile> listByParentPath(String pathName);
}
